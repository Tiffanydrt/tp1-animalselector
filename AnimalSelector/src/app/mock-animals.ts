import { Animal } from './animal';

export const ANIMALS: Animal[] = [
  { id: 1, title: 'Le chat', src:'../assets/chat.jpg', description:'Le chat est un animal capricieux.' },
  { id: 2, title: 'Le chien', src:'../assets/chien.jpg', description : 'Le chien est un animal fidèle.' }
];

