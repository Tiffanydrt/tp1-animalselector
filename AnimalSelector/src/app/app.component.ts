import { Component } from '@angular/core';
import { ANIMALS } from './mock-animals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AnimalSelector';
  src='';
  animals = ANIMALS;

  selectedAnimal:AppComponent; 
  onSelect(animal: AppComponent): void {
    this.selectedAnimal = animal;
  }

}

